<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('game.begin'));
});

Route::get('/game/begin/', 'Game\GameController@index')->name('game.begin');
Route::get('/game/start/', 'Game\GameController@start')->name('game.start');
