@extends('layouts.app')
@section('content')
    <h1 id="inf-player"></h1>
    <br/>
    <table border="2" id="click-table">
        @component('components.tableComponent')
        @endcomponent
        @component('components.tableComponent')
        @endcomponent
        @component('components.tableComponent')
        @endcomponent
    </table>
@endsection
