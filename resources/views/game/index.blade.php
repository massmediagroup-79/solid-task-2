@extends('layouts.app')
@section('content')
    @include('includes.result_messages')
    <form method="get" action="{{ route('game.start')}}">
        @csrf
        @component('components.inputComponent',['idElement' => 'player1','description' =>'Player 1:','namePlayer' =>'player1'])
        @endcomponent
        @component('components.inputComponent',['idElement' => 'player2','description' =>'Player 2:','namePlayer' =>'player2'])
        @endcomponent
        <button type="submit" class="btn btn-primary">Start</button>
    </form>
@endsection
