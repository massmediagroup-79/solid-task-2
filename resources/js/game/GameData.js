class GameData {

    constructor(UrlData)
    {
        this.urlData=UrlData;
    }
    /**
     *
     * @param url
     */
    goTo(url)
    {
        if (window.history.pushState) {
            window.history.pushState(null, null, url);
            return;
        }
        window.location.assign(url);
    }

    /**
     *
     * @returns {any}
     */
    getNumberMoves()
    {
        let numberMoves = JSON.parse(new URLSearchParams(window.location.search).get('numberMoves'));
        if (numberMoves===null) {
            return 0;
        }
        numberMoves++;
        return numberMoves;
    }
    /**
     * data 1 or 2
     * @param col
     * @param row
     * @param data
     */

    updateStatus(col, row, data)
    {
        let status = JSON.parse(new URLSearchParams(window.location.search).get('status'));
        let numberMoves = this.getNumberMoves();
        if (!status) {
            status = [[,,,],[,,,],[,,,]];
        }
        status[row][col] = data;
        this.urlData.goTo(`${window.location.search.slice(0, window.location.search.indexOf('&numberMoves'))}
        &numberMoves=${JSON.stringify(numberMoves)}
        &status=${JSON.stringify(status)}`);

    }
}
export default GameData;
