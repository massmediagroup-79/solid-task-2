class AjaxGame {
    /**
     * @param tableId
     * @param gameData
     * @param url
     */

    constructor(tableId, gameData, url)
    {
        if (JSON.parse(new URLSearchParams(window.location.search).get('win'))===0) {
            $('#inf-player').text('A draw');
            $(`${tableId} td`).off('click');
            return;
        }
        if (JSON.parse(new URLSearchParams(window.location.search).get('win'))) {
            $('#inf-player').text(`Player ${new URLSearchParams(window.location.search).get(`player${JSON.parse(new URLSearchParams(window.location.search).get('win'))}`)} win`);
            $(`${tableId} td`).off('click');
            return;
        }
        let player = 0;
        console.log(gameData.getNumberMoves());
        $(() => {
            $(`${tableId} td:not(.player1, .player2)`).on('click', function () {
                const row = $(this).parent('tr').index();
                const col = $(this).parent().children().index($(this));
                $(this).addClass(`player${player % 2 + 1}`);
                $(this).off('click');
                gameData.updateStatus(col, row, player % 2 + 1);
                player=gameData.getNumberMoves();
                if (player === 9) {
                    $('#inf-player').text('A draw');
                    url.goTo(`${window.location.search}&win=0`);
                }
                $.ajax({
                    url: '/game/start'+window.location.search,
                    type: "GET",
                    success: function (data) {
                        if (data.result) {
                            $('#inf-player').text(`Player ${new URLSearchParams(window.location.search).get(`player${data.result}`)} win`);
                            url.goTo(`${window.location.search}&win=${data.result}`);
                            $(`${tableId} td`).off('click');
                        }
                    },
                });
            });
        });
    }
}
export default AjaxGame;
