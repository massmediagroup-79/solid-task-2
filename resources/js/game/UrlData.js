class UrlData {
    /**
     *
     * @param url
     */
    goTo(url)
    {
        if (window.history.pushState) {
            window.history.pushState(null, null, url);
            return;
        }
        window.location.assign(url);
    }

}
export default UrlData;
