<?php
namespace App\Http\Controllers\Game;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateName;
use App\Services\WinnerServices;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GameController extends Controller
{
    public function index()
    {
        return view('game.index');
    }

    public function start(ValidateName $request, WinnerServices $winner)
    {
        $status = json_decode($request->get('status'));
        $result = $winner->getWinner($status);
        if ($request->isXmlHttpRequest() && $status) {
            return response()->json(['result' => $result]);
        }
        return view('game.game', [
            'player1'=>$request->get('player1'),
            'player2'=>$request->get('player2'),
            'status' => $status ?? [],
        ]);
    }
}
