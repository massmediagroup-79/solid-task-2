<?php

namespace App\Services;

class WinnerServices
{
    private function checkWinnerByMark($field, $mark)
    {
        $winRow = array_fill(0, 3, $mark);
        for ($i = 0; $i < 3; $i++) {
            if ($field[$i] === $winRow) {
                return true;
            }
        }
        for ($i = 0; $i < 3; $i++) {
            if ([$field[0][$i], $field[1][$i], $field[2][$i]] === $winRow) {
                return true;
            }
        }
        return [$field[0][0], $field[1][1], $field[2][2]] === $winRow ||
            [$field[2][0], $field[1][1], $field[0][2]] === $winRow;
    }

    public function getWinner($field)
    {
        if ($this->checkWinnerByMark($field, 1)) {
            return 1;
        }
        if ($this->checkWinnerByMark($field, 2)) {
            return 2;
        }
        return false;
    }
}
